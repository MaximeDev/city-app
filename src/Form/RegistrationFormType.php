<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'home.username', 'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'form.holderUser',
            )))
            ->add('email', TextType::class, array('label' => 'form.email', 'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'form.holderEmail',
            )))
            ->add('password', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'label' => 'home.password',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'form.holderPassword',
                ),
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'form.messagePassword',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'form.minMessagePassword {{ limit }} characteres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'form.terms',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'form.agreeTermsMessage',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
