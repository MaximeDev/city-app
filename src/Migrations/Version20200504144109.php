<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504144109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        // insert admin user
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(180) NOT NULL, reset_token VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        // insert admin user
        $this->addSql('INSERT INTO user (`id`, `username`, `roles`, `password`, `email`, `reset_token`) VALUES (1, "admin", "ROLE_ADMIN", "$2y$13$uWAsTY2mqYGAja6YWJiX..mehHxL2v77b.zma8IKDrt9DY9436BUm", "admin@admin.fr", "5zOuTdoy1ojQhEBjZudNpOE9crqh21lYYanc5Y7lAvk")');
        // Index pour la table `user`
        $this->addSql('ALTER TABLE user ADD UNIQUE KEY UNIQ_8D93D649E7927C74 (email)');
        $this->addSql('ALTER TABLE user ADD UNIQUE KEY UNIQ_8D93D649F85E0677 (username)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B023427F83F1A');
        $this->addSql('DROP INDEX CountryCode ON city');
        $this->addSql('ALTER TABLE city CHANGE Name Name CHAR(35) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE Population Population INT DEFAULT 0 NOT NULL, CHANGE CountryCode CountryCode CHAR(3) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE country MODIFY Code CHAR(3) DEFAULT \'\'\'\'\'\' NOT NULL');
        $this->addSql('ALTER TABLE country DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE country CHANGE Code Code CHAR(35) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE Name Name CHAR(52) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74 ON user');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON user');
    }
}
