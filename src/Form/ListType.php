<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Country;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ListType extends AbstractType
{
    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', TextType::class, array('label' => 'form.cityName', 'attr' => array(
            'class' => 'form-control',
        )))
        ->add('countrycode', EntityType::class, [
            'class' => Country::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')
                ->orderBy('c.code', 'ASC');
            },
            'choice_label' => 'code',
            'label' => 'form.countrycode',
            'attr' => array(
            'class' => 'form-control',
            )
        ])
        ->add('population', IntegerType::class, array('label' => 'form.population', 'attr' => array(
            'class' => 'form-control',
        )));
        $builder->add('save', SubmitType::class, array(
            'label' => 'form.save',
            'attr' => array(
            'class' => 'btn btn-primary btn-margin',
        )
        ));
    }

    /**
    * @param OptionsResolver $resolver
    */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\City',
            'route'=>null
        ));
    }
}
