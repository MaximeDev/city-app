<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder,
    GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, TokenGeneratorInterface $tokenGenerator): Response
    {
        $user = new User();
        $token = $tokenGenerator->generateToken();
        $user->setResetToken($token);
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $password = $form->get('password')->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $encoded =$encoder->encodePassword($user, $password);

            $user->setPassword($encoded);
            $user->setRoles('ROLE_USER');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $config=parse_url($_ENV['MAILER_URL']);
            // Create the Transport
            $transport = (new Swift_SmtpTransport('smtp.gmail.com',
            465,'ssl'))
            ->setUsername($config["user"])
            ->setPassword($config["pass"])
            ->setStreamOptions(array('ssl' => array('allow_self_signed'
            => true, 'verify_peer' => false)))
            ;
            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);
            // Create a message
            $message = (new Swift_Message('Confirmation d\'inscription'))
            ->setFrom(['john@doe.com' => 'John Doe'])
            ->setTo([$form->get('email')->getData()])
            ->setBody($this->renderView('mails/register.html.twig', [
            'name' => $form->get('username')->getData(),
            ]),
            'text/html')
            ;
            // Send the message
            $result = $mailer->send($message);

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
