<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\City;
use App\Entity\User;
use App\Form\ListType;

class ListController extends AbstractController
{

    /**
     * @Route("/list/listing", name="cities_listing")
     */
    public function cityListing()
    {
        $cityRepository = $this->getDoctrine()->getRepository(City::class);
        $cities = $cityRepository->findall();
        return $this->render('list/listing.html.twig', array(
            'cities' => $cities
        ));
    }

    /**
    * @Route("/list/create", name="cities_create")
    *
    * @param Request $request
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function createCity(Request $request)
    {
        $city = new City();
        $form = $this->createForm(ListType::class, $city,array());
    
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $city->setName($form['name']->getData());
            $city->setCountrycode($form['countrycode']->getData());
            $city->setPopulation($form['population']->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($city);
            $em->flush();
            return $this->redirectToRoute('cities_listing');
        }

        return $this->render('list/create.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
    * @Route("/list/edit/{id}", name="cities_edit")
    *
    * @param int $id
    * @param Request $request
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function editTask($id, Request $request)
    {
        $task = $this->getDoctrine()
        ->getRepository(City::class)
        ->findOneBy(array('id' => $id));

        $form = $this->createForm(ListType::class, $task,array());
        
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $task->setName($form['name']->getData());
            $task->setCountrycode($form['countrycode']->getData());
            $task->setPopulation($form['population']->getData());
            
            $em->flush();

            return $this->redirectToRoute('cities_listing');
        }

        return $this->render('list/create.html.twig', array(
            'task' => $task,
            'form' => $form->createView(),
        ));
    }

    /**
    * @Route("/list/delete/{id}", name="cities_delete")
    *
    * @param int $id
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function deleteTask($id)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove( $this->getDoctrine()
        ->getRepository(City::class)
        ->findOneBy(array('id' => $id) ));
        $em->flush();
        
        return $this->redirectToRoute('cities_listing');
    }
}
