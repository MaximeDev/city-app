<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504162302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B023427F83F1A FOREIGN KEY (CountryCode) REFERENCES country (Code)');
        $this->addSql('CREATE INDEX CountryCode ON city (CountryCode)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B023427F83F1A');
        $this->addSql('DROP INDEX CountryCode ON city');
        $this->addSql('ALTER TABLE city CHANGE Name Name CHAR(35) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE Population Population INT DEFAULT 0 NOT NULL, CHANGE CountryCode CountryCode CHAR(3) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
        $this->addSql('ALTER TABLE country MODIFY Code CHAR(3) DEFAULT \'\'\'\'\'\' NOT NULL');
        $this->addSql('ALTER TABLE country DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE country CHANGE Code Code CHAR(3) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE Name Name CHAR(52) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
    }
}
