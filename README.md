# Description

Application Listing de villes avec symmfony 5

## Installation

une base de données est implémentée  
via les migrations de Doctrine.

pour l'utiliser faire :
```bash
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

## Settings

Vous pouvez également utiliser votre propre base de données.  
Pour cela il suffit de :   
1 - supprimer les migrations dans "src\Migrations"
Sauf celle qui sert a créer la table user "Version20200504144109"  
2 - renommer la base de données dans le fichier .env   
"DATABASE_URL=mysql://root@127.0.0.1:3306/citydb?serverVersion=5.7"   
puis faire :
```bash
php bin/console doctrine:migrations:migrate
```

|        Migrations     |   Description    |
| --------------------- | ---------------- |
| Version20200504142021 | Création des Tables  |
| Version20200504144109 | Création de la Table User |
| Version20200504150517 | Insertion des données City |
| Version20200504160021 | Insertion des données Country |
| Version20200504162302 | Connexion entre City et Country |


### Attention
Un utilisateur Admin avec un mot de passe encrypté en bcrypt et créé par défaut
grâce au migrations

| User     |   Password    |   Encryption    |
| -------- | ------------- | --------------- |
| Admin    | adminpassword |     bcrypt      |